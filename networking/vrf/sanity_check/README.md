# VRF sanity checks
Sanity checks for VRF, making sure basic functionalities work
Test Maintainer: [Antoine Tenart](mailto:atenart@redhat.com)

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
